# Shellshape

Shellshape is a library for controling your shelly devices.
It's 100 % written in Kotlin.

## WIP

The project is currently WIP.
That means there's still a lot todo and
some things might be a bit unstable. Feel free to
[report bugs on codeberg](https://codeberg.org/reennix/shellshape/issues).
