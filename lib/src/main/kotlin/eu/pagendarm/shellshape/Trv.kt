package eu.pagendarm.shellshape

import eu.pagendarm.shellshape.dataclasses.TrvStatus
import eu.pagendarm.shellshape.util.HttpClientFactory
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.request.*

class ConnectionFailedException : Exception()

const val GET_TEMP_PATH = "status"

/**
 * Gernal TRV interface, declares methods for TRV communication
 */
interface Trv {
    suspend fun setTemperature(targetTemp: UInt) {
        throw NotImplementedError()
    }

    suspend fun getTemperature(): Float {
        throw NotImplementedError()
    }

    suspend fun getValvePos(): Float {
        throw NotImplementedError()
    }

    suspend fun getStatus(): TrvStatus {
        throw NotImplementedError()
    }
}

/**
 * A class that handles TRV connection over HTTP
 *
 * NOTE:
 *
 * HttpTrv does currently not run with CIO. Default engine is OkHttp.
 * This might change in the future.
 *
 *
 * @param httpClient - An httpClient with (json) serialization enabled
 * @param apiKey - A shelly api key, can be left empty if authentication is not enabled
 * @param baseUrl - Either the device url (e.g. "http://192.168.1.2/") or shelly cloud url
 */
class HttpTrv(
    httpClient: HttpClient = HttpClientFactory.newJsonOkHttpClient(), apiKey: String = "", baseUrl: String = ""
) : Client(httpClient = httpClient, apiKey = apiKey, baseUrl = baseUrl), Trv {
    /**
     * Get the TRV status
     */
    override suspend fun getStatus(): TrvStatus {
        // Repeating request as TRV might not respond sometimes
        for (i in 0..10) {
            try {
                return httpClient.get("${baseUrl}/${GET_TEMP_PATH}").body()
            } catch (_: Exception) {
            }
        }
        throw ConnectionFailedException()
    }

    /**
     * Set the TRV temperature to the target temperature
     * @param targetTemp - Desired temperature
     */
    override suspend fun setTemperature(targetTemp: UInt) {
        for (i in 0..10) {
            httpClient.get("${baseUrl}/thermostats/0?target_t=${targetTemp}")
            return
        }
        throw ConnectionFailedException()
    }


    /**
     * Gets the current temperature measured with thermostat 0
     *
     * @return Measured temperature in percent
     */
    override suspend fun getTemperature(): Float {
        return getStatus().Thermostats[0].Temp.Value
    }

    /**
     * Get TRV valve position in percent
     */
    override suspend fun getValvePos(): Float {
        return getStatus().Thermostats[0].Pos
    }
}
