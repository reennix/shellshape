package eu.pagendarm.shellshape

import io.ktor.client.*

/**
 * A client that contains default methods for all shelly devices.
 */
abstract class Client(
    val httpClient: HttpClient,
    val apiKey: String,
    val baseUrl: String,
) {

}
