package eu.pagendarm.shellshape

import eu.pagendarm.shellshape.dataclasses.PlugRelay
import eu.pagendarm.shellshape.dataclasses.PlugStatus
import eu.pagendarm.shellshape.util.HttpClientFactory
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.request.*

interface PlugS {
    /**
     * Toggles the Shelly Plug (S) relay.
     *
     * @return Power state
     */
    suspend fun togglePower(relayId: UInt): Boolean { throw NotImplementedError() }

    /**
     * Get the power state of a Shelly Plug (S)
     */
    suspend fun getPowerState(relayId: UInt): Boolean { throw NotImplementedError() }

    /**
     * Get the status of a Shelly Plug (S)
     */
    suspend fun getStatus(): PlugStatus { throw NotImplementedError() }
}
/**
 * Shelly Plug S client
 */
class PlugSHttp(httpClient: HttpClient = HttpClientFactory.newJsonCIOClient(), apiKey: String = "", baseUrl: String = "") :
    Client(httpClient = httpClient, apiKey = apiKey, baseUrl), PlugS {
    override suspend fun togglePower(relayId: UInt): Boolean {
        return (httpClient.get("$baseUrl/relay/$relayId?turn=toggle").body() as PlugRelay).IsOn
    }

    override suspend fun getPowerState(relayId: UInt): Boolean {
        return (httpClient.get("$baseUrl/relay/$relayId").body() as PlugRelay).IsOn
    }

    override suspend fun getStatus(): PlugStatus {
        return (httpClient.get("$baseUrl/status").body() as PlugStatus)
    }
}