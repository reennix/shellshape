package eu.pagendarm.shellshape.dataclasses

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class TrvStatus(
    @SerialName("thermostats") val Thermostats: List<TrvThermostat>,
)

@Serializable
data class TrvThermostat(
    @SerialName("pos") val Pos: Float,
    @SerialName("target_t") val TargetTemp: TrvTargetTemp,
    @SerialName("tmp") val Temp: TrvTemp,
    @SerialName("schedule") val Schedule: Boolean,
    @SerialName("schedule_profile") val ScheduleProfile: Int,
    @SerialName("boost_minutes") val BoostMinutes: Int,
    @SerialName("window_open") val WindowOpen: Boolean,
)

@Serializable
data class TrvTargetTemp(
    @SerialName("enabled") val Enabled: Boolean,
    @SerialName("value") val Value: Float,
    @SerialName("value_op") val ValueOp: Float,
    @SerialName("units") val Units: String,
)

@Serializable
data class TrvTemp(
    @SerialName("value") val Value: Float,
    @SerialName("units") val Units: String,
    @SerialName("is_valid") val IsValid: Boolean,
)