package eu.pagendarm.shellshape.dataclasses

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class PlugSettings(
    @SerialName("max_power") val MaxPower: Int,
    @SerialName("led_power_disabled") val LedPowerDisabled: Boolean,
    @SerialName("relays") val Relays: List<PlugRelay>,
)

@Serializable
data class PlugRelay(
    @SerialName("has_timer") val HasTimer: Boolean,
    @SerialName("ison") val IsOn: Boolean,
    @SerialName("overpower") val Overpower: Boolean,
)

@Serializable
data class PlugStatus(
    @SerialName("relays") val Relays: List<PlugRelay>,
    @SerialName("meters") val Meters: List<PlugMeter>,
    @SerialName("temperature") val Temp: Float,
    @SerialName("overtemperature") val OverTemp: Boolean,
)

@Serializable
data class PlugMeter(
    @SerialName("power") val Power: Float,
    @SerialName("overpower") val Overpower: Float,
    @SerialName("is_valid") val IsValid: Boolean,
    @SerialName("timestamp") val Timestamp: Long,
    @SerialName("counters") val Counters: List<Float>,
    @SerialName("total") val Total: Int,
)