package eu.pagendarm.shellshape.util

import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.engine.okhttp.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.serialization.kotlinx.json.*
import kotlinx.serialization.json.Json

/**
 * Factory class for HttpClients
 */
class HttpClientFactory {
    companion object {
        /**
         * Generates a new HttpClient with json content negotiation enabled. It uses the CIO engine.
         * Content negotiation ignores unknown keys.
         *
         * @return Ktor HttpClient
         */
        fun newJsonCIOClient(): HttpClient {
            return HttpClient(CIO) {
                install(ContentNegotiation) {
                    json(json = Json {
                        ignoreUnknownKeys = true
                    })
                }
            }
        }

        /**
         * Generates a new HttpClient with josn content negotiation enabled. It uses the OkHttp engine.
         * Content negotiation ignores unknown keys.
         *
         * return Ktor HttpClient
         */
        fun newJsonOkHttpClient(): HttpClient {
            return HttpClient(OkHttp) {
                install(ContentNegotiation) {
                    json(json = Json {
                        ignoreUnknownKeys = true
                    })
                }
            }
        }
    }
}